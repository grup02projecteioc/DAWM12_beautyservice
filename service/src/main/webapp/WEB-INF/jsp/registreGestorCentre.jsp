<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
      <html>

      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <jsp:useBean id="usuariGestor" class="beauty.service.service.repositori.Entitats.Usuari" scope="session">
        </jsp:useBean>
        <title>Registre Gestor de Centre</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/registre.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="/js/validacioFormularis.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/css/validacioFormularis.css">
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>        

      </head>

      <body>

        <div class="menu-btn">
          <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">
          <!-- Header -->
          <header id="home-header">
            <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

            <nav class="main-nav">
              <ul class="main-menu">
                <li><a href="/">Home</a></li>
                <li><a href="quiSom">Qui Som</a></li>
                <li><a href="buscarReserva">Reservar tractament</a></li>
              </ul>
              <ul class="right-menu">
                <li><a href="login">Login</a></li>
                <li><a href="tipusUsuari">Registre</a></li>
              </ul>
            </nav>
          </header>

          <!--Main-->
          <main>

            <div class="container-main">

              <div class="welcome">
                <img src="/img/welcome.jpg" alt="welcome">
              </div>

              <form:form class="form-horizontal" id="registreGestorCentre" modelAttribute="usuariGestor"
                action="registreGestorProcess" method="POST">
                <div class="card">
                  <div class="card-header">
                    <h3>Registre Gestor de Centre</h3>
                  </div>

                  <div class="card-body">

                    <div class="form-group">
                      <form:label path="nom" class="control-label col-sm-4">Nom:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="nom" name="nom" id="nom" required="required" />
                      </div>
                    </div>

                    <div class="form-group">
                      <form:label path="cognoms" class="control-label col-sm-4">Cognoms:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="cognoms" name="cognoms" id="cognoms"
                          required="required" />
                      </div>
                    </div>

                    <div class="form-group">
                      <form:label path="nomUsuari" class="control-label col-sm-4">Nom d'usuari:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="nomUsuari" name="nomUsuari" id="nomUsuari"
                          required="required" />
                      </div><span>${missatgeNomUsuari}</span>
                    </div>

                    <div class="form-group">
                      <form:label path="mail" class="control-label col-sm-4">E-mail:</form:label>
                      <div class="col-sm-8">
                        <form:input type="email" class="validate form-control" path="mail" name="mail" id="mail"
                          required="required" />
                      </div><span>${missatgeMail}</span>
                    </div>

                    <div class="form-group">
                      <form:label path="contrasenya" class="control-label col-sm-4">Contrasenya</form:label>
                      <div class="col-sm-8">
                        <form:input type="password" class="validate form-control" path="contrasenya" name="contrasenya"
                          id="contrasenya" required="required" />
                      </div><span>${missatgeContrasenya}</span>
                    </div>

                    <div class="form-group">
                      <form:label path="ciutat" class="control-label col-sm-4">Ciutat:</form:label>
                      <div class="col-sm-8">
                        <form:input type="text" class="validate form-control" path="ciutat" name="ciutat" id="ciutat"
                          required="required" />
                      </div>
                    </div>

                    <div class="form-group">
                      <form:label path="codiPostal" class="control-label col-sm-4">Codi postal:</form:label>
                      <div class="col-sm-8">
                        <form:input type="number" class="validate form-control" path="codiPostal" name="codiPostal" id="codiPostal"
                          required="required" />
                      </div>
                    </div>

                    <div class="form-group">
                      <form:input type="hidden" path="rol" name="rol" value="GESTOR" />
                    </div>


                    <div class="form-group">
                      <div class="col-sm-4"><form:button id="registre" name="registre" class="btn">Registrarse</form:button></div>
                        <div class="col-sm-4"><a href="/" class="btn">Home</a></div>
                          <div class="col-sm-4"><a href="login" class="btn">Iniciar sessio&#769;</a></div>
                      
                    </div>


                  </div>
                </div>
              </form:form>
            </div>          

            <!--Footer-->
            <footer id="main-footer" class="footer">
              <div class="footer-inner">
                <div><img src="/img/Dark-logo.png" class="logo"></div>
                <ul>
                  <li><a href="#">Contacte</a></li>
                  <li><a href="#">Politica de la Web</a></li>
                  <li><a href="#">&copy; Beauty Service 2022</a></li>
                </ul>
              </div>

            </footer>

        </div>

      </body>

      </html>