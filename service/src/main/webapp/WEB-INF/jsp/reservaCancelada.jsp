<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
      <html>

      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <jsp:useBean id="usuariGestor" class="beauty.service.service.repositori.Entitats.Usuari" scope="session">
        </jsp:useBean>
        <title>Registre Gestor de Centre</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/reservaCancelada.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="/js/validacioFormularis.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/css/validacioFormularis.css">
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/home.js" type="text/javascript"></script>
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>
        <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>

      </head>

      <body>

        <div class="menu-btn">
          <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">
          <!-- Header -->
          <header id="home-header">
            <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

            <nav class="main-nav">
              <ul class="main-menu">
                <li><a href="/">Home</a></li>
                <li><a href="quiSom">Qui Som</a></li>
                <li><a href="buscarReserva">Reservar tractament</a></li>
              </ul>
              <ul class="right-menu">
                <li><a href="lesMevesDades">Hola ${nomUsuari}</a></li>
                <li><i class="fa-solid fa-user"></i></li>
                <li><a href="tipusUsuari">Registre</a></li>
              </ul>
            </nav>
          </header>

          <!--Main-->
          <main>
            <div class="container-main">


              <!-- Responsive Profile Menu-->
              <div class="profile-menu">
                <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                <div id="myDropdown" class="dropdown-content">
                  <a href="buscarReserva">Cercar un servei per reservar</a>
                  <a href="lesMevesReserves">Les meves reserves</a>
                  <a href="lesMevesDades">Les meves dades</a>
                  <a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a>
                  <a href="baixa">Donar-te de baixa</a>
                </div>
              </div>

              <div class="wrapper">

                <!-- My Profile Menu -->

                <section id="lateral-menu">
                  <div>
                    <nav class="main-menu">
                      <ul class="menuLateral">
                        <li><a href="buscarReserva">Cercar un servei per reservar</a></li>
                        <li><a href="lesMevesReserves">Les meves reserves</a></li>
                        <li><a href="lesMevesDades">Les meves dades</a> </li>
                        <li><a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a></li>
                        <li><a href="baixa">Donar-te de baixa</a></li>
                      </ul>
                    </nav>
                  </div>
                </section>

                <!-- Missatge Reserva Cancelada -->

                <section class="title">
                  <div>
                    <h4>Reserva cancelada!</h4>
                    <div>
                      <a href="buscarReserva" class="btn">Reservar un altre servei</a><br>
                      <a href="lesMevesReserves" class="btn">Tornar a les meves reserves</a><br>
                    </div>
                </section>

              </div>


              <!--seccio consentiment de banner-->
              <div class="cookie-banner">
                <div class="cookie-close accept-cookie"></div>
                <div class="container">
                  <p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament dels
                    nostres
                    serveis.
                    <br>Si accepteu o continueu navegant, considerem que accepteu les condicions.<br>
                    <a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf"> Per m&#233;s
                      infomaci&#243; sobre
                      la protecci&#243; de dades</a>
                  </p>
                  <button type="button" class="btn acceptar-cookie">Aceptar</button>
                </div>
              </div>

              <!-- Banner -->
              <section class="banner_section">
                <img src="/img/Experiencia_relax.jpg" alt="Reserva Cancelada" class="banner_centres">
              </section>

            </div>
          </main>

          <!--Footer-->
          <footer id="main-footer" class="footer">
            <div class="footer-inner">
              <div><img src="/img/Dark-logo.png" class="logo"></div>
              <ul>
                <li><a href="#">Contacte</a></li>
                <li><a href="#">Politica de la Web</a></li>
                <li><a href="#">&copy; Beauty Service 2022</a></li>
              </ul>
            </div>
          </footer>

        </div>

      </body>

      </html>