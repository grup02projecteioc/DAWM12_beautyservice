<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/quiSom.css">
        <script src="/js/home.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>

        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
        <title>Qui Som</title>
    </head>

    <body>
        <!-- Responsive menu -->

        <div class="menu-btn">
            <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">
            <!-- Header -->
            <header id="home-header">
                <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                <nav class="main-nav">
                    <ul class="main-menu">
                        <li><a href="/">Home</a></li>
                        <li><a href="quiSom">Qui Som</a></li>
                        <li><a href="buscarReserva">Reservar tractament</a></li>
                    </ul>
                    <ul class="right-menu">
                        <li><a href="benvingut">${logat}</a></li>
                        <li><a href="login">${login}</a></li>
                        <li><a href="tipusUsuari">${registre}</a></li>
                        <li><a href="logout">${tancarSessio}</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Main area-->
            <main>



                <h1 class="title">Qui Som?</h1>


                <div class="mission">

                    <div class="textMission">
                        <h2 class="subtitle">T’has plantejat mai…</h2>
                        <p>Quantes vegades posposem la visita a la nostra perruqueria del barri per què no tenim temps
                            de
                            trucar o anar a reservar cita? Quants cops no anem a la perruqueria de moda perquè no sabem
                            quant ens costarà el servei que volem?</p>
                        <p>Per aquests i més motius hem creat una web en la que, fàcilment, podràs reservar cita,
                            comprovar
                            el preu que et costarà el servei i consultar els salons de bellesa que més s’ajustin al teu
                            estil.</p>
                    </div>
                    <img src="/img/our mission.jpg" alt="Le nostre missió" class="imatge">


                </div>
                <div class="about">
                    <img src="/img/this_that.jpg" alt="Saló o client" class="imatge">
                    <div class="text">

                        <div class="textAbout">
                            <p>I si ets un saló... Hi ha molts clients que no et coneixen encara perquè no et veuen a
                                Internet,
                                no tens la visibilitat que et mereixes. La nostra web et permetrà fàcilment obrir-te a
                                un nou
                                públic; i a més nosaltres sempre estarem en contacte per ajudar-te en qualsevol cosa que
                                necessitis.</p>

                            <p>Has pensat en quantes hores has passat al telèfon i capgirant pàgines per prendre nota i
                                ordenar
                                visites? Això pot fàcilment deixar de passar perquè mentre atens a clients, les reserves
                                s’agafen soles! Sols hauràs de llistar els horaris disponibles i dedicar-te al que
                                millor saps
                                fer i que els teus clients valoren més. </p>
                            <p>Només necessitem que et donis d’alta a la nostra web! Et demanarem les dades mínimes que
                                necessitem per a què en pocs minuts comencis a gaudir dels avantatges i la comoditat que
                                t’oferim amb la nostra plataforma.</p>

                        </div>


                    </div>
                </div>
                <h2>No t’ho pensis més i entra!</h2>

                <section id="Home-section-c" class="home-section-c">
                    <div class="content">
                    </div>
                </section>


            </main>

            <!--Footer-->
            <footer id="main-footer" class="footer">
                <div class="footer-inner">
                    <div><img src="/img/Dark-logo.png" class="logo"></div>
                    <ul>
                        <li><a href="#">Contacte</a></li>
                        <li><a href="#">Politica de la Web</a></li>
                        <li><a href="#">&copy; Beauty Service 2022</a></li>
                    </ul>
                </div>

            </footer>

        </div>

    </body>

    </html>