package beauty.service.service.repositori;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import beauty.service.service.repositori.Entitats.Serveis;

/**Interfície que crida a CrudRepository */
@Repository
public interface CrudRepositoriServeis extends CrudRepository<Serveis, Integer> {
 
}
