package beauty.service.service.repositori;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import beauty.service.service.repositori.Entitats.Salo;

/**
 * Classe que representa el repositori de la classe Salo
 */
@Repository
public class RepositoriSalo {
    
    @Autowired
    private CrudRepositoriSalo crudRepositoriSalo;

   /**
    * Mètode rep com a paràmetre un identificador d'un saló y que el retorna d'entre
    * tots els salons guardats a la base de dades
    * @param id
    * @return un saló o null si no el troba
    */ 
   public Salo  getSaloById(int id) {
    Iterable<Salo> allSalons = crudRepositoriSalo.findAll();
    for(Salo s: allSalons) {
        if(s.getIdSalo() == id) {
            return s;
        }
    }
    return null; 
   }

    /**
     * Mètode que crida un mètode de CrudRepositoriSalo per recuperar
     * tots els salons guardats a la base de dades.
     * @return llistat salons
     */
    public Iterable<Salo> getAllSalons() {
        return crudRepositoriSalo.findAll();
    }
    
    /**
     * Mètode que amb l'id d'usuari retorna tots els salons de bellesa
     * guardats en la base de dades associats a ell
     * @param idUsuari
     * @return llistat salons filtrats per id de l'usuari gestor
     */
    public List<Salo> getAllSalonsByIdUsuariGestor(int idUsuari) {
        Iterable<Salo> allSalons = getAllSalons();
        List<Salo> salonsById = new ArrayList<>();
        for (Salo s: allSalons) {
            int idUsuariSalo = s.getUsuari().getId();
            if(idUsuariSalo == idUsuari) {
                salonsById.add(s);
            }
        }

        return salonsById;
    }

    /**
     * Mètode que rep com a paràmetre un llistat de salons i un String del nom del saló.
     * Si el nom del saló es troba entre el llistat, retorna el saló
     * @param salonsUsuari
     * @param nomSalo
     * @return un saló o null si no el troba
     */
    public Salo getSaloByNom(List<Salo> salonsUsuari, String nomSalo) {
        for(int i=0; i<salonsUsuari.size(); i++) {
            if(salonsUsuari.get(i).getNomSalo().equals(nomSalo)) {
                return salonsUsuari.get(i);
            }
        }
        return null;
    }

    /**
     * Mètode que rep com a paràmetre un llistat de salons i un String de la direcció.
     * Si la direcció del saló es troba entre el llistat, retorna el saló
     * @param salonsUsuari
     * @param direccio
     * @return un saló o null si no el troba
     */
    public Salo getSaloByDireccio(List<Salo> salonsUsuari, String direccio) {
        for(int i=0; i<salonsUsuari.size(); i++) {
            if(salonsUsuari.get(i).getDireccio().equals(direccio)) {
                return salonsUsuari.get(i);
            }
        }
        return null;
    }

}
