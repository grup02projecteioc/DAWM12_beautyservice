package beauty.service.service.repositori.Entitats;


import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <h1>Classe que representa una entitat de tipus {@link Salo} </h1>
 *Aquesta classe gestiona les dades d'un {@link Salo} , al mateix temps estableix una relació
  entre l'entitat {@link Usuari} a partir de la clau forana idUsuari , i un altre a l'entitat {@link TipusServeis} a partir de  
  la clau forana {@link #idTipusServei}
 */
@Entity
@Table(name="salo")
public class Salo implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    @JoinColumn(name = "idUsuari")
    private Usuari usuari;

    @OneToMany(mappedBy = "salo", fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    private Set<Ofertes> Ofertes;

    
    @OneToOne(mappedBy = "salo", fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    private TipusServeis tipusServeis;

    @Id
    @NotNull
    @Column(name = "idSalo")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Size(max=9)
    private int idSalo;

    @NotNull
    @Column(name = "direccio")   
    private String direccio;

    @NotNull
    @Column(name = "cp")
    private String codiPostal;

    @NotNull
    @Column(name = "ciutat")
    private String ciutat;

    @NotNull
    @Column(name = "nomSalo")
    private String nomSalo;

    @NotNull
    @Column(name = "idTipusServei")
    private String  idTipusServei;

    @NotNull
    @Column(name = "numTreballadors")
    private int  numTreballadors;



    /**
     *  Constructor buit, que s'utilitzarà en la creació d'un objecte que serà l'instància d'una classe. Generalment
     *  duu a terme les operacions requerides per inicialitzar la classe abans que els mètodes siguin invocats
     *  o s'accedeixi als metòdes.
     */
    public Salo() {}
    
    /** 
     * Mètode per optenir l'id d'un tipus de servei
     * @return String Retorna un objecte tipus String, que representa la clau forana de l'entitat TipusServeis.
     */
    public String getIdTipusServei() {
        return this.idTipusServei;
    }

    
    /** 
     * Aquesta funció es fa servir per establir l'id de la clau farana {@link #idTipusServei} del a classe TipusSeveis
     * @param idTipusServei Valor tipus String, la qual determina l'id a actualizar
     */
    public void setIdTipusServei(String idTipusServei) {
        this.idTipusServei = idTipusServei;
    }

    /** 
     * Mètode que ens ajuda a obtenir un objecte de TipusServeis dins la ralación entre la taula Salo i TipusServei
     * @return TipusServeis Ens retorna un objecte de TipusServeis, dins la cadena de relacions de a taula Salo i TipusServeis.
     */
    public TipusServeis getTipusServeis() {
        return this.tipusServeis;
    }

    
    /** 
     * La següent funció afegeix o actualitza una clau forana que relaciona Salo i TipusServeis.
     * @param tipusServeis Aquest paràmetre per actualitzar un objecte TipusSevies.
     */
    public void setTipusServeis(TipusServeis tipusServeis) {
        this.tipusServeis = tipusServeis;
    }

    
    /** 
     * Mètode que obté el número de treballadors, que cada saló pot tenir.
     * @return int Ens retorna un valor de tipus enter, que serà el número de treballador de cada saló.
     */
    public int getNumTreballadors() {
        return this.numTreballadors;
    }

    
    /** 
     * Aquesta funció actualizarà o afigira un número determirminat de treballadors a cada saló
     * @param numTreballadors Es passarà per paràmetre un valor tipus enter, el qual actualitzara a la tupla
     * {@link #numTreballadors} a partir d'un valor donat.
     */
    public void setNumTreballadors(int numTreballadors) {
        this.numTreballadors = numTreballadors;
    }


    
    /** 
     * Mètode que gestiona la clau forana Usuari, dins la relació entre Salo i Usuari.
     * @return Usuari Aquesta funció ens retornarà un objecte de tipus Usuari.
     */
    public Usuari getUsuari() {
        return this.usuari;
    }

    
    /** 
     * Funció que la farem servir per obtenir en nom d'un saló
     * @return String Retorna un objecte de tipus String, que en aquest cas serà el nom del saló
     */
    public String getNomSalo() {
        return nomSalo;
    }

    
    /** 
     * Aquest mètode ens ajudara a actualitzar o guardar el nom d'un saló, passant-li per 
     * paràmetre un objecte de tipus String, que serà qui determinara el nom del saló.
     * @param nomSalo Aquest paràmetre de de tipus String serà qui canviarà  el nom de determinat saló.
     */
    public void setNomSalo(String nomSalo) {
        this.nomSalo = nomSalo;
    }

    
    /** 
     * Mètode que ens ajudara a canviar un objecte de tipus Usuari, passant-li aquest com un criteri a actualitzar.
     * @param usuari Aquest paràmetre, determinara un usuari a actualitzar o guardar.
     */
    public void setUsuari(Usuari usuari) {
        this.usuari = usuari;
    }


    
    /** 
     * Aquesta funció gestiona la relació de Un a Molts entre Salo i Ofertes fent referencia a l'idOfertes del l'entitat Ofertes, al mateix 
     * temps ens retorna un llista de objectes de tipus Ofertes
     * @return Set<Ofertes> Ens retorna una llista de objectes de tipus Ofertes
     */
    public Set<Ofertes> getOfertes() {
        return this.Ofertes;
    }

    
    /**
     * Dins la relació d'Un a Molts entre Salo i Ofertes, aquesta funció actualitza una llista de objectes de tipus Ofertes.
     *  
     * @param Ofertes Paràmetre d'una llista de de Ofertes que actualitza amb una llista d'Ofertes.
     */
    public void setOfertes(Set<Ofertes> Ofertes) {
        this.Ofertes = Ofertes;
    }

    
    /** 
     * Mètode que ens retorna l'identificador d'un Salo, obtenint un valor de tipus enter
     * @return int Ens retorna l'id d'un {@link Salo}
     */
    public int getIdSalo() {
        return idSalo;
    }

    
    /** 
     * Funció que ens ajuda a actualitzar l'atribut id de la classe Saló, passant-li per paràmetre un idSalo.
     * @param idSalo Paràmetre de tipus enter, el qual guardarà un idSalo, dins l'entitat {@link Salo}.
     */
    public void setIdSalo(int idSalo) {
        this.idSalo = idSalo;
    }

    
    /** 
     *Mètode per poder accedir la propietat direcció de la classe {@link Salo}.
     * @return String Ens retorna un objecte de tipus String, que serà la propietat {@link #direccio} de la classe {@link Salo}
     */
    public String getDireccio() {
        return direccio;
    }

    
    /** 
     * Aquest setter actualitza un atribut {@link #direccio} dins la classe {@link Salo}
     * @param direccio Paràmetre de tipus String, el qual guardarà un atribut {@link #direccio}
     */
    public void setDireccio(String direccio) {
        this.direccio = direccio;
    }

    
    /** 
     * Aquesta funció ens perment accedir a la propietat {@link #codiPostal} de la classe {@link Salo}
     * @return String S'opte la propietat {@link #codiPostal} amb un valor de tipus String.
     */
    public String getCodiPostal() {
        return codiPostal;
    }

    
    /** 
     * Mètode que ens ajuda a canviar en la classe {@link Salo} la propietat {@link #codiPostal} 
     * @param codiPostal Paràmetre de tipus String per poder canviar l'atribut {@link #codiPostal}
     */
    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }

    
    /** 
     * Getter per obtenir l'atribut {@link #ciutat} obtenint un valor de tipus String.
     * @return String Ens retorna el valor de la propietat {@link #ciutat} 
     */
    public String getCiutat() {
        return ciutat;
    }

    
    /** 
     * Funció per poder modificar el valor {@link #ciutat} passant-li per paràmetre un objecte de tipus String.
     * @param ciutat Aquest paràmetre ens ajuda a actulitzar l'atribut {@link #ciutat}
     */
    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    
}
