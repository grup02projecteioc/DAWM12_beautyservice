package beauty.service.service.repositori;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import beauty.service.service.repositori.Entitats.Usuari;

/**
 * Classe que representa el repositori de la classe Usuari
 */
@Repository
public class RepositoriUsuari {
    @Autowired
    private CrudRepositoriUsuari crudRepositori;

    /**
     * Mètode que rep com a paràmetre un String que representa el nom d'usuari, 
     * i busca a la base de dades l'usuari amb aquest nom
     * @param username Paràmetre que es fa servir per trobar un usuari pel seu username
     * @return Usuari o null si no el troba
     */
    public Usuari getUsuariByUsername(String username) {
        Iterable<Usuari> allUsuaris = crudRepositori.findAll();
        for(Usuari u: allUsuaris) {
            if(u.getNomUsuari().equals(username) && u.isActive()==true) {
                return u;
            }
        }
        return null;
    }

    /**
     * Mètode que rep com a paràmetre un String que representa l'email
     * de l'usuari, i que l'utilitza per recuperar l'usuari que tingui
     * aquesta dada associada a la base de dades
     * @param mail Paràmete per trobar un usuari pel seu mail
     * @return un Usuari o null si no el troba
     */
    public Usuari getUsuariByMail(String mail) {
        Iterable<Usuari> allUsuaris = crudRepositori.findAll();
        for(Usuari u: allUsuaris) {
            if(u.getMail().equals(mail)) {
                return u;
            }
        }
        return null;      
    }

    /**
     * Mètode que rep com a paràmetre l'identificador d'un usuari i que l'utilitza
     * per recuperar un usuari de la base de dades
     * @param id Identificar per trobar un usuari a partir d'un id donat
     * @return un Usuari o null si no el troba
     */
    public Usuari getUsuariById(int id) {
        Iterable<Usuari> allUsuaris = crudRepositori.findAll();
        for(Usuari u: allUsuaris) {
            if(u.getId() == id) {
                return u;
            }
        }
        return null;        
    }


    
    /** 
     * Funció que retorna un usuari a partir del seu estat, que pot ser actiu o inactiu
     * @param username El nom d l'usuari a trobar
     * @return Usuari Retorna un objecte tipus Usuari
     */
    public Usuari getUsuariByUsernameActius(String username) {
        Iterable<Usuari> allUsuaris = crudRepositori.findAll();
        for(Usuari u: allUsuaris) {
            if(u.getNomUsuari().equals(username)) {
                return u;
            }
        }
        return null;
    }
        
        /** 
         * Mètode que dana de baixa  un usuari mitjançant un username donat
         * @param userName El user name de l'usuari a donar de baixa
         */
        public void danarDeBaixaSalo(String userName){
            Usuari usuariBaixa =getUsuariByUsernameActius(userName);
            if((usuariBaixa!=null) && usuariBaixa.getRol().equals("GESTOR")){
                usuariBaixa.setActive(false);
            }
        }
        
        
        /** 
         * Funció que comprava l'estat d'un usuari a partir del seu nom d'usuari
         * @param userName  Nom d'usuari a trobar
         * @return boolean Ens diu si un usuari esta actiu o no
         */
        //Mètode que comprava l'estat de un usuari
        public boolean isUsuariGestorActive(String userName){
            Usuari usuari =getUsuariByUsernameActius(userName);
            boolean isActive=false;
            if((usuari!=null) && usuari.getRol().equals("GESTOR") && usuari.getActive()==true){
                isActive=true;
            }
            return isActive;
        }

}

