package beauty.service.service.service;


import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import beauty.service.service.repositori.CrudRepositoriSalo;
import beauty.service.service.repositori.RepositoriSalo;
import beauty.service.service.repositori.Entitats.Salo;

@Service
@Transactional
public class SaloService {
    
    @Autowired
    private CrudRepositoriSalo crudRepositoriSalo;

    @Autowired
    private RepositoriSalo repositoriSalo;

    /***
     * Mètode que retorna tots els salons guardats a la base de dades
     * mitjançant un mètode del repositori
     * @return llistat de tots els salons
     */
    public Iterable<Salo> getAllSalons() {
        return repositoriSalo.getAllSalons();
    }

    /***
     * Mètode que retorna tots els salons guardats a la base de dades i 
     * associats a un id d'usuari concret mitjançant un mètode del repositori
     * @param idUsuari Id per saber el salo a partir d'un usuari
     * @return llistat del salons filtrat per un id d'usuari
     */
    public List<Salo> getSalonsByIdUsuari(int idUsuari) {
        return repositoriSalo.getAllSalonsByIdUsuariGestor(idUsuari);
    }

    /***
     * Mètode que guarda un saló a la base de dades cridant un mètode del
     * repositori
     * @param salo Objecte de tipus Salo a guardar a la base de dades
     */
    public void guardarSalo(Salo salo) {
        crudRepositoriSalo.save(salo);
    }

        /***
     * Mètode que retorna un saló pel seu nom entre una llista de salons 
     * @param salonsUsuari Llistat de tipus Salo
     * @param nomSalo Nom de salo a trobar
     * @return
     */
    public Salo getSaloByNom(List<Salo> salonsUsuari, String nomSalo) {
        return repositoriSalo.getSaloByNom(salonsUsuari, nomSalo);
    }

    /***
     * Mètode que retorna un saló pel seu ID
     * @param id L'id per buscar un salo
     * @return Ens retorna un objecte tipus Salo
     */
    public Salo getSaloById(int id) {
        return repositoriSalo.getSaloById(id);
    }

    /***
     * Mètode que retorna un saló per la seva direcció
     * @param salonsUsuari El que pertany a un usuari
     * @param direccio La direcció d'un usuari
     * @return En retorna un objete tipus Salo, passan-li per paràmetre un salo d'usuari i la
     * seva direcció
     */
    public Salo getSaloByDireccio(List<Salo> salonsUsuari, String direccio) {
        return repositoriSalo.getSaloByDireccio(salonsUsuari, direccio);
    }


}
