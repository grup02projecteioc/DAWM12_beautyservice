package beauty.service.service.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import beauty.service.service.repositori.CrudRepositoriReserves;
import beauty.service.service.repositori.RepositoriReserves;
import beauty.service.service.repositori.Entitats.Reserves;

/**
 * Classe per gestionar les reserver d'un service
 */
@Service
@Transactional
public class ReservaService {
    
    @Autowired
    private CrudRepositoriReserves crudRepositoriReserves;

    @Autowired
    private RepositoriReserves repositoriReserves;

    
    /** 
     * Funció que guarda una reserva a la base de dades
     * @param reserva Objecte tipus reserva a guardar
     */
    public void guardarReserva(Reserves reserva) {
        crudRepositoriReserves.save(reserva);
    }

    
    /** 
     * Mèdode que guarda una reserva per un id donat
     * @param idReserva  Id que identifica a la reserva a guardar
     * @return Reserves Ens retorna un objecte tipus reserva guardat
     */
    public Reserves getReservaById(int idReserva) {
        return repositoriReserves.getReservaById(idReserva);
    }

    
    /** 
     * Funció que ens retorna una llista amb totes les reserves disponibles
     * @return List<Reserves> LLista de reserves disponibles
     */
    public List<Reserves> allReserves() {
        return (List<Reserves>)repositoriReserves.getAllReserves();
    }

    
    /** 
     * Mèdode que guarda una reserve en base a un identificador.
     * @param idSalo Identificar per saber que reserva s'esta guardant
     * @return List<Reserves> Ens retorna una lista una llista filtrat per un id
     */
    public List<Reserves> getReservesByIdSalo(int idSalo) {
        return repositoriReserves.getReservesByIdSalo(idSalo);
    }

    
    /** 
     * Función que obté un llistat de reserves en base a un id d'un client
     * @param idClient Id del client a filtrar
     * @return List<Reserves> Retorna una lista de reserves a partir de un id del client
     */
    public List<Reserves> getReservesByIdClient(int idClient) {
        return repositoriReserves.getReservesByIdClient(idClient);
    }

    
    /** 
     * Mètode que ens retorna un llistat de reserves a partir d'un llistat tipus Reserve i  estat en la qual se encuentre la
     * la reserva
     * @param reservesClient Llistat del la reserva del client
     * @param estat Situación en la cual es troba la reserva
     * @return List<Reserves> Se opté un llista de les reserves a partir d'un estat, passan-li per
     * paràmetre un llitat de tipus reserves
     */
    public List<Reserves> getReservesByEstat(List<Reserves> reservesClient, int estat) {
        return repositoriReserves.getReservesByEstat(reservesClient, estat);
    }

    
    /** 
     * Función que retorna un llistat de objetes tipus Reserves antigues des un id d'un client
     * @param idClient Indentificar per poder estableir un filtrat de reserves antigues
     * @return List<Reserves> Llistat de reserves antigues a partir de un id d'un client
     */
    public List<Reserves> getReservesAntiguesByClient(int idClient) {
        return repositoriReserves.getReservesAntiguesByClient(idClient);
    }
}

