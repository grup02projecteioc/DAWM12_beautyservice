package beauty.service.service.controller;

import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import beauty.service.service.repositori.Entitats.Reserves;
import beauty.service.service.repositori.Entitats.Salo;
import beauty.service.service.repositori.Entitats.Usuari;
import beauty.service.service.service.ReservaService;
import beauty.service.service.service.SaloService;
import beauty.service.service.service.UsuariService;

/**
 * Classe que representa el controlador que gestiona el login
 * de l'usuari
 */
@Controller
public class LoginController {
    
    @Autowired
    private UsuariService service;

    @Autowired
    private SaloService serviceSalo;

    @Autowired
    private ReservaService serviceReserves;

   /***
     * Mètode que recupera l'usuari logat mitjançant la classe Principal i envia les dades
     * d'aquest a la vista que es mostrarà
     * @param model
     * @param principal
     * @return jsp "lesMevesDades"
     */
    @GetMapping(value="/lesMevesDades")
    public String dadesUsuariLogat(ModelMap model, Principal principal) {
        String nomusuariLogat = principal.getName();
        Usuari usuariLogat = service.getUsuariByUsername(nomusuariLogat);
        model.addAttribute("idUsuari", usuariLogat.getIdUsuari());
        model.addAttribute("nomUsuari", usuariLogat.getNomUsuari());
        model.addAttribute("nom", usuariLogat.getNom());
        model.addAttribute("cognoms", usuariLogat.getCognoms());
        model.addAttribute("mail", usuariLogat.getMail());
        model.addAttribute("ciutat", usuariLogat.getCiutat());
        model.addAttribute("codiPostal", usuariLogat.getCodiPostal());
        model.addAttribute("rol", usuariLogat.getRol());

        return "lesMevesDades";
    }

    /***
     * Mètode que recupera les dades de l'usuari mitjançant la classe Principal. Comprova quin
     * rol té assignat l'usuari, si 'client' o 'gestor', i actua en conseqüència, o bé retornant 
     * la pàgina de benvinguda que correspon al client o bé retornant la pàgina de benvinguda que
     * correspon al gestor. Abans, introdueix a la vista les dades que corresponen a les reserves que té
     * (en el cas del client) o els salons que té registrats (en cas de gestor)
     * @param model
     * @param principal
     * @return vista 'benvingut' o vista 'benvingutGestor'
     */
    @GetMapping(value="/benvingut")
    public String benvingut(ModelMap model, Principal principal) {
        Usuari usuari = service.getUsuariByUsername(principal.getName());
        if (usuari.getRol().equals("GESTOR")) {
            model.addAttribute("nomUsuari", usuari.getNomUsuari());
            List<Salo> salonsUsuari = serviceSalo.getSalonsByIdUsuari(usuari.getIdUsuari());
            model.addAttribute("salons", salonsUsuari);

            return "benvingutGestor";
        } 

        List<Reserves> reserves = serviceReserves.getReservesByIdClient(usuari.getIdUsuari());
        List<Reserves> reservesConfirmades = serviceReserves.getReservesByEstat(reserves, 1);
        List<Reserves> reservesPendents = serviceReserves.getReservesByEstat(reserves, 0);
        List<Reserves> reservesDescartades = serviceReserves.getReservesByEstat(reserves, 2);


        model.addAttribute("nomUsuari", usuari.getNomUsuari());
        model.addAttribute("idUsuari", usuari.getIdUsuari());
        model.addAttribute("reservesConfirmades", reservesConfirmades);
        model.addAttribute("reservesPendents", reservesPendents);
        model.addAttribute("reservesDescartades", reservesDescartades);
        model.addAttribute("reserves", reserves);

        return "benvingut";
    }

    /**
     * Mètode que recupera el nom d'usuari de l'usuari logat, l'introdueix en una vista 
     * que li servirà per donar-se de baixa i la retorna
     * @param model
     * @param principal
     * @return vista 'baixa'
     */
    @GetMapping(value="/baixa")
    public String baixa(ModelMap model, Principal principal) {
        model.addAttribute("nomUsuari", principal.getName());

        return "baixa";
    }

    /**
     * Mètode que recupera l'usuari logat y processa la seva baixa, assignant el seu estat com
     * a no-actiu
     * @param model
     * @param principal
     * @return vista 'baixaEfectiva'
     */
    @GetMapping(value="/baixaProcess")
    public String baixaProcess(ModelMap model, Principal principal) {
        Usuari usuariBaixa = service.getUsuariByUsername(principal.getName());
        usuariBaixa.setActive(false);;

        return "baixaEfectiva";
    }
    
}
