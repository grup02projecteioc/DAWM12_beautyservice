/*
window.addEventListener('load', afegirUsuari);

function afegirUsuari(){
var usuariInput=document.getElementById('usuariId').value;
gravaCookie("usuariNom",usuariInput);
}
*/


/**
*  separa els diferents cookies de document.cookie
*
* @return {Array<String>} vector amb els diferents cookies de la forma clau = valor
*/
function recuperaArrayCookies(){
	return document.cookie.split("; ");
}

/**
*   cerca en un vector de cookies el valor que correspon al segon parametre
*
* @param {Array<String>} nomBuscat - clau de la cookie que es cerca dinsel vector
*
* @return {Array<String>} el valor que correspon a la clau indicada pel parametre nomBuscat
*   o null si no existeix aquesta clau en el vector.
*/
function obteCookie(nomBuscat){
    var temp;
    var nomCookie;
    var valorCookie;
	var arrayCookies = recuperaArrayCookies();
	console.log(arrayCookies);
	for(var i=0; i< arrayCookies.length;i++){

		temp = arrayCookies[i].split("=");

		nomCookie = temp[0];

		valorCookie = temp[1];

		if(nomCookie==nomBuscat){
			return (valorCookie);
		}

	}
	return null;
 
}

/**
*   Cerca en un vector de cookies el valor que correspon al segon parametre
* @param {Array<String>} nomBuscat - clau de la cookie que es cerca dinsel vector
* @return {Array<String>} el valor que correspon a la clau indicada pel parametre nomBuscat
*/
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
	  var c = ca[i].trim();
	  if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
	}
	return "";
  }

/**
* Emmagatzema les cookies amb el nom i valor indicats als paràmetres
* i el moment actual com ultim acces;
*
* @param {string} nomCookie - nom de la cookie a guardar
* @param {string} valor_cookie - valor de la cookie a guardar
*/
function gravaCookie(nomCookie,valorCookie){
	var anysCookies=20;
	var dataExpirar = new Date();
	dataExpirar.setFullYear(dataExpirar.getFullYear()+anysCookies);
	var textExpirar = " expires="+dataExpirar.toString();
	document.cookie = nomCookie+" = "+valorCookie+"; "+textExpirar;
}


/**
* Emmagatzema les cookies amb el nom i valor indicats als paràmetres
* amb una data de caducitat
*
* @param {string} cvalor - nom de la cookie a guardar
* @param {string} nom - valor de la cookie a guardar
* @param {string} exdays - numero de dies en expirar
*/
function gravaCookieAmbDataExpirar(cnom, cvalor, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toGMTString();
	document.cookie = cnom + "=" + cvalor + "; " + expires;
  }


/**
*   Esborra la cookie amb la clau corresponent al parametre
*   @param {string} nomCookie - clau de la cookie a esborrar
*/
function esborraCookie(nomCookie){
    document.cookie = nomCookie+" =; expires=01 Jan 2000 00:00:00 UTC";

  
}
